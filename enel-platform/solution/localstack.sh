#!/bin/bash


# Init
workspacePath="/workspaces/glin-${SOLUTION_CODE}-lan-${SOLUTION_NAME}"


# Start Localstack
cd "${workspacePath}/localstack"
docker-compose up
docker-compose down