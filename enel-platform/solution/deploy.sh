#!/bin/bash


# Init
workspacePath="/workspaces/glin-${SOLUTION_CODE}-lan-${SOLUTION_NAME}"
dockerWorkspacePath="${workspacePath}/containers/$(basename $(ls -d ${workspacePath}/containers/*/ | head -n 1))"


# Build and Start Docker container
cd "${dockerWorkspacePath}"
docker build --rm -t ${SOLUTION_NAME} .
docker run --rm -it -p "8080:8080" -p "8787:8787" --env-file "${workspacePath}/.devcontainer/env" --name ${SOLUTION_NAME} ${SOLUTION_NAME}