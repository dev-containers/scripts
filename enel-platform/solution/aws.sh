#!/bin/bash


# Init
workspacePath="/workspaces/glin-${SOLUTION_CODE}-lan-${SOLUTION_NAME}"
export AWS_ACCESS_KEY_ID=test
export AWS_SECRET_ACCESS_KEY=test
export AWS_DEFAULT_REGION=eu-central-1


# AWS
cd "${workspacePath}/localstack"
aws --endpoint-url=http://host.docker.internal:4566 --region=eu-central-1 secretsmanager create-secret --name secretname
aws --endpoint-url=http://host.docker.internal:4566 secretsmanager put-secret-value --secret-id secretname --secret-string file://secret.json
aws --endpoint-url=http://host.docker.internal:4566 --region eu-central-1 iam create-role --role-name ruolo --assume-role-policy-document file://policy.json