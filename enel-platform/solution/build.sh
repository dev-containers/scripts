#!/bin/bash


# Init
workspacePath="/workspaces/glin-${SOLUTION_CODE}-lan-${SOLUTION_NAME}"


# Build Solution
cd "${workspacePath}"
chmod +x build_script.sh
./build_script.sh