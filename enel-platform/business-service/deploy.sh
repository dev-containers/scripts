#!/bin/bash


# Init
workspacePath="/workspaces/glin-${BUSINESS_SERVICE_CODE}-lan-${BUSINESS_SERVICE_NAME}"


# Build Application
mvn clean install -Dmaven.test.skip=true -f "${workspacePath}/infrastructure/platform/events"
mvn clean install -Dmaven.test.skip=true -f "${workspacePath}/src/${BUSINESS_SERVICE_MODULE_NAME}"
mvn clean package -Dmaven.test.skip=true -f "${workspacePath}/src/${BUSINESS_SERVICE_MODULE_NAME}CompositeApplication"


# Build Docker image
docker build -t "${BUSINESS_SERVICE_NAME}" -f "${HOME}/scripts/enel-platform/business-service/Dockerfile" --build-arg BUSINESS_SERVICE_MODULE_NAME=${BUSINESS_SERVICE_MODULE_NAME} --build-arg COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION=${COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION} --build-arg SYNAPSE_PLATFORM_SDK_VERSION=${SYNAPSE_PLATFORM_SDK_VERSION} --build-arg WSO2_RUNTIME_VERSION=${MICRO_INTEGRATOR_VERSION} "${workspacePath}"


# Start Docker container
docker run --rm -it -p "8290:8290" --env-file "${workspacePath}/.devcontainer/env" --name "${BUSINESS_SERVICE_NAME}" "${BUSINESS_SERVICE_NAME}"